from django.db import models


class Profile(models.Model):
    external_id = models.PositiveIntegerField(verbose_name='ID пользователя', null=True)
    name = models.CharField(verbose_name='Имя пользователя', max_length=60, null=True)

    def __str__(self):
        return f'#{self.external_id} {self.name}'

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


class Message(models.Model):
    profile = models.ForeignKey(
        to='bot.Profile',
        verbose_name='Профиль',
        on_delete=models.CASCADE,
        null=True,
    )
    text = models.TextField(
        verbose_name='Текст',
        null=True,
    )
    created_at = models.DateTimeField(
        verbose_name='Время получения',
        auto_now_add=True,
        null=True,
    )

    def __str__(self):
        return f'Сообщение {self.pk} от {self.profile}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

