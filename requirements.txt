asgiref==3.5.2
Django==4.1.4
pip==21.3.1
setuptools==60.2.0
sqlparse==0.4.3
tzdata==2022.7
wheel==0.37.1
python-telegram-bot==13.15
